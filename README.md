# Thesis

## Name
Greg Wheildon Thesis Scripts

## Description
This project displays the final code used to generate data and plots throughout my thesis. The scripts are split by data chapter into folders, and within each chapter folder into seperate analysis folders. The scripts are not exhaustive of all the analysis attempted during my PhD but represent those analyses included in the final document.


## Support
For further information please email g.t.wheildon2@exeter.ac.uk




