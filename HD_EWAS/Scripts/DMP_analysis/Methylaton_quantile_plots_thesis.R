#   #   #   #   #   #   #   #   #   # 
#   #   #   #   #   #   #   #   #   #  
######### Introduction ##############
#   #   #   #   #   #   #   #   #   #  
#   #   #   #   #   #   #   #   #   # 

# title: "EWAS_methylation Quantile plots"
# author: "Greg Wheildon (gw386@exeter.ac.uk)"
# date: 17/09/23
# Aim: To explore beta value distribution between controls and HD 

###INPUTS
# betas == beta matrix
# pheno == pheno file with a grouping collumn names "Group" and a sample index collumn in the form "Sentrix_ID_Position"
# groups == a named vector with group names of the Group collumn you want to calculate e.g c("AD","CTRL")
# Ncores == number of cores to parallelise with

# You also need tidyr, ggplot2,parallel 


###Load packages 
library(methylumi)
library(wateRmelon)
library(tidyr)
library(ggplot2)
library(parallel)

setwd("/mnt/data1/Greg/HD_EWAS/QC")

load("HD_EWAS_Mset_normalised_betas_Striatum.rdat")

load("HD_Normalised_Striatum.rdat")

pheno <- pheno_Striatum

pheno$Group <- pheno$Phenotype
pheno$Sentrix_ID_Position <- pheno$Basename

groups <- c("Control", "HD")


createQuantileMat <- function(betas,pheno,groups,Ncores){
  
  #check pheno has a collumn called "Group"
  if(!any(colnames(pheno) == "Group")){
    return("You need a pheno collumn called `Group`, do better next time")
  }else{
    #Make a median tester
    varTest <- function(x){
      med <- median(x)
      return(c(med))
    } 
    #using parallel processing, calulate global median
    cl<- makeCluster(Ncores)
    medProbes<-t(parApply(cl,betas, 1, varTest))
    stopCluster(cl)
    #calculate quantiles for the global median and assign to each probe
    quants<- as.numeric(ggplot2::cut_number(medProbes,n=10))
    
    #make a summary matrix
    summaryFrame <- matrix(medProbes,ncol = 1,nrow = nrow(betas))
    rownames(summaryFrame) <- rownames(betas)
    summaryFrame <- cbind(summaryFrame,quants)
    colnames(summaryFrame) <- c("medianAll","quants")
    
    for(g in groups){
      #get a grouping index
      groupIndex <- as.character(pheno[which(pheno$Group == g),"Sentrix_ID_Position"])
      
      #Calculate median per group
      cl<- makeCluster(Ncores)
      tempMed <- t(parApply(cl,betas[,groupIndex[which(groupIndex %in% colnames(betas))]], 1, varTest))
      stopCluster(cl)
      
      #append your summary matrix
      summaryFrame <- cbind(summaryFrame, tempCol = as.numeric(tempMed))
      colnames(summaryFrame)[which(colnames(summaryFrame) == "tempCol")] <- g
      assign("quantileSummary",summaryFrame,envir = parent.frame())
    }
    
    
    
  }
}

# Example of running and transforming for plotting on the first 100 rows of myy beta matrix
# 
#createQuantileMat(betas = betas[1:100,],pheno = pheno, groups = groups,32)
#sumDat <- as.data.frame(quantileSummary[,c(2:4)])


#specLong <- sumDat %>% 
 # pivot_longer(-quants)

#### run on full set of betas 

createQuantileMat(betas = betas,pheno = pheno, groups = groups,32)
sumDat <- as.data.frame(quantileSummary[,c(2:4)])


specLong <- sumDat %>% 
  pivot_longer(-quants)


png("beta_methylation_quantile plots_stri.png", width=2400, height=1500, res=300)
ggplot(specLong,aes(x = as.factor(quants), y = value, fill = as.factor(name)))+
 scale_fill_discrete(name="Group",     ##### scale_xxxx_xxxx has many forms - colour is for lines and points, fill for solid areas eg boxplot
                         labels=c("Control", "HD"))+
  #geom_violin()+
  geom_boxplot(outlier.size=0.1)+
  ggtitle("Striatum")+
  ylab("ß-value")+
    xlab("Quantile")
  dev.off()





  
load("HD_EWAS_Mset_normalised_betas_Entorhinal_cortex.rdat")
load("HD_Normalised_Entorhinal_cortex.rdat")

pheno <- pheno_Entorhinal_cortex
pheno$Group <- pheno$Phenotype
pheno$Sentrix_ID_Position <- pheno$Basename


  #### run on full set of betas 

createQuantileMat(betas = betas,pheno = pheno, groups = groups,32)
sumDat <- as.data.frame(quantileSummary[,c(2:4)])


specLong <- sumDat %>% 
  pivot_longer(-quants)


png("beta_methylation_quantile plots_EC.png", width=2400, height=1500, res=300)
ggplot(specLong,aes(x = as.factor(quants), y = value, fill = as.factor(name)))+
 scale_fill_discrete(name="Group",     ##### scale_xxxx_xxxx has many forms - colour is for lines and points, fill for solid areas eg boxplot
                         labels=c("Control", "HD"))+
  
  geom_boxplot(outlier.size=0.1)+
  ylab("ß-value")+
  ggtitle("Entorhinal Cortex")+
    xlab("Quantile")
  dev.off()




load("HD_EWAS_Mset_normalised_betas_Cerebellum.rdat")
load("HD_Normalised_Cerebellum.rdat")

pheno <- pheno_Cerebellum 
pheno$Group <- pheno$Phenotype
pheno$Sentrix_ID_Position <- pheno$Basename



  #### run on full set of betas 

createQuantileMat(betas = betas,pheno = pheno, groups = groups,32)
sumDat <- as.data.frame(quantileSummary[,c(2:4)])


specLong <- sumDat %>% 
  pivot_longer(-quants)


png("beta_methylation_quantile plots_Cer.png", width=2400, height=1500, res=300)
ggplot(specLong,aes(x = as.factor(quants), y = value, fill = as.factor(name)))+
 scale_fill_discrete(name="Group",     ##### scale_xxxx_xxxx has many forms - colour is for lines and points, fill for solid areas eg boxplot
                         labels=c("Control", "HD"))+
  
  geom_boxplot(outlier.size=0.1)+
  ylab("ß-value")+
  ggtitle("Cerebellum")+
    xlab("Quantile")
  dev.off()